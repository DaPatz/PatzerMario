'use strict';

var images;
var mario;
var bg;
var stars = [];
var obstacles = [];
var GROUNDLEVEL = 360;
var STARLEVEL = 220;
var life = 3;
var lifeShow;
var score = 0;

function preload() {
	var imgBackground = loadImage("assets/background.jpg");
	var imgMario = loadImage("assets/mario.jpg");
	var imgObstacle = loadImage("assets/obstacle.jpg");
    var imgStar = loadImage("assets/star.jpg");
    images = {
		background: imgBackground,
		mario: imgMario,
		obstacle: imgObstacle,
        star: imgStar
	};
    console.log("end of preload");
}

function setup() {
    createCanvas(800, 500);
    mario = new Mario();
	bg = new Background();
    createObjects();
    console.log("end of setup");
}

function draw() {
	bg.draw();
    lifeShow = life -1;
    textSize(32);
    text("Leben: " + lifeShow, 600, 50);
    text("Punkte:" + score, 600, 100);
    objects();
    mario.draw();
}

function keyPressed() {
  mario.jump();
}

function objects() {
    for(var i=0; i<stars.length; i++){
        stars[i].draw();
        if (stars[i].box.isCollission(mario.box)) {
            console.log("collission: " + mario.box.toString() + ", " + stars[i].box.toString());
            stars[i].box.x = random(width, 2*width);
            stars[i].box.y = random(GROUNDLEVEL, STARLEVEL);
            score += 10;
        }
    }
    for(var i=0; i<obstacles.length; i++){
        obstacles[i].draw();
        if (obstacles[i].box.isCollission(mario.box)) {
            console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
            if (life > 1 ) {
                life--;
            } else {
                life = 0;
                background(0);
                fill(255, 0,0);
                text("GAME OVER", 300, 250);
                text("Punkte: " + score, 300, 300);
                noLoop();
            }
            obstacles[i].box.x = random(width, 2*width);
        }
    }
}

function createObjects() {
    for(var i=0; i<5; i++)
        obstacles.push(new Obstacle());
    for(var i=0; i<5; i++)
        stars.push(new Star());
    console.log("end of setup");
}

function mousePressed() {
    obstacles = [];
    stars = [];
    createObjects();
    life = 3;
    score = 0;
    fill(0);
    loop();
    return false;
}