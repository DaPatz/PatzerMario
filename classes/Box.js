/**
 *
 * @param x x-coordinate top left corner of the box
 * @param y y-coordinate top left corner of the box
 * @param w width of the box
 * @param h height of the box
 * @constructor creates a box with these parameters.
 *                  more precisely the top left and right corner and the bottom left corner of the box.
 *              the collision method is created, which checks if there are any overlaps in the x- and y-coordinates of
 *                  2 different boxes.
 *              the toString method return the parameters of the box.
 */
function Box(x, y, w, h){
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
}

Box.prototype.topRight = function() {return this.x + this.w;};
Box.prototype.bottomLeft = function() {return this.y + this.h;};
Box.prototype.left = function() {return this.x;};
Box.prototype.top = function() {return this.y;};

Box.prototype.isCollission = function (box){ 
	return ((this.topRight() > box.left()) &&
            (box.topRight() > this.left()) &&
            (this.bottomLeft() > box.top()) &&
            (box.bottomLeft() > this.top()));
}

Box.prototype.toString = function (){
	return "x:" + this.x + ", y:" + this.y + ", w:" + this.w + ", h:" + this.h;
}