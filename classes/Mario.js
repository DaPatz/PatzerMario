'use strict';
/**
 *
 * @constructor first creates a new box with a x-coordinate of 50, a y-coordinate with the value of groundlevel
 * 				    and the width and height with the value of size.
 * 				then the jump method is declared, which manipulates the y-coordinate of Mario.
 * 				then a draw method is called, which moves the box depending on the jump method. When Mario jumps he climbs
 * 			        with a rate of -2 until he reaches his jumpHeight then he falls with a rate of 2(original 4).
 * 			        last the image of Mario is drawn into the box again with the width and height with the value of size.
 */
function Mario() {
	var jumpHeight = 200;
	var size = 50;
	this.box = new Box(50, GROUNDLEVEL, size, size);
	var velocityY = 0;
	this.jump = function () {
		console.log("jump");
		velocityY = -2;
	}

	this.draw = function () {
		this.box.y = this.box.y + velocityY;
		if (this.box.y < jumpHeight)
			velocityY = 2;
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}