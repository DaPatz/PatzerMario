/**
 *
 * @constructor draws the background with a movement in the x-direction.
 *              inside the draw method the x-coordinate where the background should be drawn next is calculated with
 *                  (old x-coordinate -1) divided with rest by width.
 *                  then the image of the background is drawn with this x-coordinate and a y-coordinate of 0.
 *
 */
function Background() {
	var posX = 0;
	
	this.draw = function () {
        posX = (posX - 1) % width;
		image(images.background, posX, 0);
	}
}