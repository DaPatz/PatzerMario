'use strict';
/**
 *
 * @constructor first creates a new box with a random x-coordinate between width and 2*width,
 * 					y-coordinate groundlevel and the width and height of the obstacle image.
 * 				then a draw method is called which moves the obstacle at its x-coordinate with a rate of-1
 * 					until it is out of the canvas.
 * 			    	then its x-coordinate will be assigned with a new value again a random between width and 2*width.
 * 			    	last the image of the obstacle is drawn into the box.
 */
function Obstacle(){
	this.box = new Box(random(width, 2*width), GROUNDLEVEL, images.obstacle.width, images.obstacle.height); 
	this.draw = function () {
		this.box.x = this.box.x - 1;
		if (this.box.x < 0) 
			this.box.x = random(width, 2*width);
		image(images.obstacle, this.box.left(), this.box.top());
	}
}